local fcarrier, fcareer, frealm, currobj
local scobjectivelist = {}
local AnnouncedTime = 0;
local ANNOUNCE_DELAY = 1
local timetoannounce = ANNOUNCE_DELAY


local function SetCarrier( realm, nameofflag ,tagspecial) -- set up carrier windows
	Trakario.Log( "[rkothSetCarrier] Enter realm="..tostring(realm).." nameofflag="..tostring(nameofflag))
	local labelColor, flagTex
	frealm = realm

	if ( frealm == GameData.Realm.ORDER ) then
		labelColor = DefaultColor.RealmColors[1]
		flagTex = "FlagOrder"
	elseif ( frealm == GameData.Realm.DESTRUCTION ) then
		labelColor = DefaultColor.RealmColors[2]
		flagTex = "FlagDestruction"
	else
		labelColor = DefaultColor.RealmColors[0]
		flagTex = "FlagNeutral"
	end
	
		if (tagspecial) then
			flagTex = flagTex.."-Burning"
		end	
	
	Trakario.Log( "[rkothSetCarrier] flagTex="..flagTex )
	DynamicImageSetTextureSlice( Trakario.Window.."OrderIcon", flagTex )
    DynamicImageSetTexture( Trakario.Window.."OrderIcon2", "", 0, 0 )
	LabelSetText( Trakario.Window.."OrderText", nameofflag )
	LabelSetTextColor( Trakario.Window.."OrderText", labelColor.r, labelColor.g, labelColor.b )
		
	Trakario.Log( "[rkothSetCarrier] Exit" )
end

-----------------------------------------------------
function Trakario.RKOTHUpdateTime(elapsed)
-----------------------------------------------------
	timetoannounce = timetoannounce - elapsed
    if timetoannounce > 0 then
        return
    end
    timetoannounce = ANNOUNCE_DELAY
	
	if (AnnouncedTime == 0) then
		LabelSetText( Trakario.Window.."OrderTime", L"" )			
	else 
	    local timeLeft = AnnouncedTime - GameData.ScenarioData.timeLeft
	    local text = TimeUtils.FormatClock(90 - timeLeft)
	    LabelSetText( Trakario.Window.."OrderTime", text )	
	end

end

-----------------------------------------------------
function Trakario.StartRKOTH()
-----------------------------------------------------
	Trakario.Log( "[StartRKOTH] Enter" )
	-- populate windows with initial data
	SetCarrier( L"neutral", L"", nil )
    
    Trakario.RegisterTempEvent( SystemData.Events.SHOW_ALERT_TEXT, "Trakario.RKOTHFlagTagged" )
    Trakario.RegisterTempEvent( TextLogGetUpdateEventId( "Chat" ) , "Trakario.RKOTHFlagSwitch")
	Trakario.RegisterTempEvent( SystemData.Events.PLAYER_OBJECTIVES_UPDATED, "Trakario.RKOTHFlagCarrierObj")
	Trakario.RegisterTempEvent( SystemData.Events.OBJECTIVE_OWNER_UPDATED, "Trakario.RKOTHFlagCarrierObj")
	Trakario.RegisterTempEvent( SystemData.Events.UPDATE_PROCESSED, "Trakario.RKOTHUpdateTime")
	
	-- Show the Order frame, we use it in this scenario
	WindowSetShowing( Trakario.Window.."Order", true )
	
	Trakario.RKOTHUpdateObjectives()
    
	--reset AnnouncedTime
    AnnouncedTime = 0
	Trakario.Log( "[StartRKOTH] Exit" )
end

-----------------------------------------------------
function Trakario.RKOTHUpdateObjectives()
-----------------------------------------------------
	-- as seperate function for later localisation of all BO objectives

    if (not GameData.Player.isInScenario)
    then
        return
    end
	
	local bo_names = {} --mapping for all bo names for all languages

	-- Blood of the Black Cairn
	bo_names["5150"] = {L"Fountain",L"Fontaine",L"Springbrunnen",L"Fontana",L"Fuente"}
	bo_names["5151"] = {L"Orchard",L"Verger",L"Obstgarten",L"Frutteto",L"Huerto"}
	bo_names["5152"] = {L"Estate",L"Domaine",L"Anwesen",L"tenuta",L"Finca"}
	
	--Reikland Hills
	bo_names["7019"] = {L"Fallen Bridge",L"Pont effondr�",L"Zerst�rte Br�cke",L"Ponte Caduto",L"Puente destruido"}
	bo_names["7020"] = {L"Factory",L"Fabrique",L"Fabrik",L"Fabbrica",L"f�brica"}
	bo_names["7021"] = {L"Mill",L"Moulin",L"M�hle",L"Mulino",L"molino"}

    local objectiveData = GetGameDataObjectives()

	scobjectivelist = {}
	
    for _, data in pairs(objectiveData) do
        if (data.name ~= L"")
        then	
			local item = {}
			item.id = data.id
			item.name = data.name
			item.names = bo_names[tostring(data.id)]
			item.realm = data.controllingRealm
			item.points = data.curControlPoints
            table.insert(scobjectivelist,item)
        end
    end
end 


-----------------------------------------------------
function Trakario.RKOTHFlagCarrierObj()
-----------------------------------------------------
	Trakario.Log( "[RKOTHFlagCarrierObj] Enter" )
	
	Trakario.RKOTHUpdateObjectives()
	
		for _, data in pairs(scobjectivelist) do
		if (data.realm ~= 0) and (currobj == data.id) then
			SetCarrier (data.realm,data.name)
			Trakario.Log( "[RKOTHFlagCarrierObj] Setting Carrier based on currentobj"..tostring(data.name) )
			return
		end
	end
	
	Trakario.Log( "[RKOTHFlagCarrierObj] Exit" )
	
end

-----------------------------------------------------
function Trakario.RKOTHFlagTagged()
-----------------------------------------------------
	local aType = SystemData.AlertText.VecType[1]
	local aText = SystemData.AlertText.VecText[1]
	Trakario.Log( "[RKOTHFlagTagged] Enter" )
	
	Trakario.RKOTHUpdateObjectives()
	
	if ( aType == Trakario.Alert ) then
		Trakario.Log( "[RKOTHFlagTagged] Alert="..tostring(aText) )
	end
	
	for _, data in pairs(scobjectivelist) do
		if ( aType == Trakario.Alert ) then
			--check if the flag is in the alertmessage, and mark the flag as tagged
			for _, name in pairs(data.names) do
				if ( wstring.match( wstring.lower(aText), wstring.lower(name)) ) then
					if (currobj == data.id) then
                        Trakario.Log( "[RKOTHFlagTagged] Found BO name in alert message using it"..tostring(name))
                        local realm = Trakario.StringRealmToID(aText)
                        if (realm ~= 0 and data.realm == 0) then
                        
                            SetCarrier(realm,data.name,true)
                        end	
                    end
					return
				end						
			end
		end
	end

	Trakario.RKOTHFlagCarrierObj()
	
	Trakario.Log( "[RKOTHFlagTagged] Exit" )
end


-----------------------------------------------------
function Trakario.RKOTHFlagSwitch(updateType, filterType)
-----------------------------------------------------
    if ( updateType ~= SystemData.TextLogUpdate.ADDED ) then return end
    if ( filterType ~= 24 ) then return end

	Trakario.Log( "[RKOTHFlagSwitch] Enter" )
	
	Trakario.RKOTHUpdateObjectives()
    
    local aText
    local num = TextLogGetNumEntries("Chat") - 1
    _, _, aText = TextLogGetEntry("Chat", num)

    if (type(aText) ~= "wstring") then
    
        aText = towstring(aText);
    end   

	for _, data in pairs(scobjectivelist) do
        --check if the flag is in the chat message
        for _, name in pairs(data.names) do
            if ( wstring.match( wstring.lower(aText), wstring.lower(name)) ) then
                currobj = data.id				
                Trakario.Log( "[RKOTHFlagSwitch] Found BO name in alert message using it"..tostring(name))
                AnnouncedTime = GameData.ScenarioData.timeLeft
                SetCarrier(realm,data.name, false)
                return
            end						
		end
	end

	Trakario.RKOTHFlagCarrierObj()
	
	Trakario.Log( "[RKOTHFlagSwitch] Exit" )
end
