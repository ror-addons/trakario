local carrier = {}
local wArray = {}
local W_MAX = 2
local DelayTime = 0
local REFRESH_DELAY = 0.5

local function Draw()
	Trakario.Log( "[MB3Draw] Enter" )
	local labelColor, flagTex, wlabel
	for i=0,W_MAX do
			Trakario.Log( "[MB3Draw] index="..tostring(i) )
			Trakario.Log( "[MB3Draw] player="..tostring(wArray[i].player).." careerId="..tostring(wArray[i].careerId).." realmId="..tostring(wArray[i].realmId) )
		if ( wArray[i].realmId == GameData.Realm.ORDER ) then
			labelColor = DefaultColor.RealmColors[1]
			flagTex = "FlagOrder"
		elseif ( wArray[i].realmId == GameData.Realm.DESTRUCTION ) then
			labelColor = DefaultColor.RealmColors[2]
			flagTex = "FlagDestruction"
		else
			labelColor = DefaultColor.RealmColors[0]
			flagTex = "FlagNeutral"
		end
		if ( i == 0 ) then
			wlabel = "Order"
		elseif ( i == 1) then
			wlabel = "Destruction"
		else
			wlabel = "Third"
		end
		Trakario.Log( "[MB3Draw] wlabel="..wlabel.." flagTex="..flagTex )
		DynamicImageSetTextureSlice( Trakario.Window..wlabel.."Icon", flagTex )
		LabelSetText( Trakario.Window..wlabel.."Text", wArray[i].player )
		LabelSetTextColor( Trakario.Window..wlabel.."Text", labelColor.r, labelColor.g, labelColor.b )
		Trakario.SetCareerIcon( Trakario.Window..wlabel.."Icon2", wArray[i].careerId )
	end
	Trakario.Log( "[MB3Draw] Exit" )
end

local function Sort()
	Trakario.Log( "[MB3Sort] Enter" )
	
	local tmpArray  = {
		[0] = { player = L"", careerId = nil, realmId = GameData.Realm.NONE, deaths=0 },
		[1] = { player = L"", careerId = nil, realmId = GameData.Realm.NONE, deaths=0 },
		[2] = { player = L"", careerId = nil, realmId = GameData.Realm.NONE, deaths=0 },
	}	
	
	for i=0, W_MAX do
		if ( wArray[i].player ~= L"" ) then
			for j=0, W_MAX do
				if ( tmpArray[j].player == L"" ) then
					tmpArray[j] = wArray[i]
					break
				end
			end
		end	
	end
	wArray = tmpArray
	Trakario.Log( "[MB3Sort] Exit" )
end

local function Erase( index )
	Trakario.Log( "[MB3Erase] Enter index="..tostring(index).." player="..tostring(wArray[index].player) )
	wArray[index].player = L""
	wArray[index].careerId = nil
	wArray[index].realmId = GameData.Realm.NONE
	wArray[index].deaths = 0
	Trakario.Log( "[MB3Erase] Exit index="..tostring(index).." player="..tostring(wArray[index].player) )
end

local function DropCarrier( carrier )
	Trakario.Log( "[MB3DropCarrier] Enter carrier="..tostring(carrier) )
	for i=0,W_MAX do
		if ( wArray[i].player == carrier ) then
			Erase( i )
			break
		end
	end
	Sort()
	Draw()
	Trakario.Log( "[MB3DropCarrier] Exit" )
end

local function SetCarrier( realm, carrier, careerId )
	Trakario.Log( "[MB3SetCarrier] Enter realm="..tostring(realm).." carrier="..tostring(carrier).." careerId="..tostring(careerId) )
	for i=0,W_MAX do
		if ( wArray[i].player == L"" ) then
			Trakario.Log( "[SetCarrier] setting index="..tostring(i) )
			wArray[i].player = carrier
			wArray[i].careerId = careerId
			wArray[i].deaths = Trakario.GetDeathCountByName(carrier)
			wArray[i].realmId = Trakario.StringRealmToID( realm )
			break
		end
	end
	Sort()
	Draw()
	Trakario.Log( "[MB3SetCarrier] Exit" )
end

-----------------------------------------------------
function Trakario.StartMB3()
-----------------------------------------------------
	Trakario.Log( "[StartMB3] Enter" )
	wArray = {
		[0] = { player = L"", careerId = nil, realmId = GameData.Realm.NONE, deaths=0 },
		[1] = { player = L"", careerId = nil, realmId = GameData.Realm.NONE, deaths=0 },
		[2] = { player = L"", careerId = nil, realmId = GameData.Realm.NONE, deaths=0 },
	}	
	Draw()
	Trakario.RegisterTempEvent( SystemData.Events.SHOW_ALERT_TEXT, "Trakario.MB3FlagCarrier" )
	
	-- Show the Order frame, we use it in this scenario
	WindowSetShowing( Trakario.Window.."Order", true )	
	WindowSetShowing( Trakario.Window.."Destruction", true )
	WindowSetShowing( Trakario.Window.."Third", true )
	Trakario.Log( "[StartMB3] Exit" )
end

-----------------------------------------------------
function Trakario.MB3FlagDrop(elapsed)
-----------------------------------------------------
	Trakario.Log( "[MB3FlagDrop] Enter ")

	DelayTime = DelayTime - elapsed
    if DelayTime > 0 then
        return
    end
    DelayTime = REFRESH_DELAY
	
	if ( not GameData.Player.isInScenario ) then
		UnregisterEventHandler(SystemData.Events.UPDATE_PROCESSED, "Trakario.MB3FlagDrop")
		Trakario.Log( "[MB3FlagDrop] Exit return nil" )
		return nil
	end
	
	local dropuser = L""
	for i=0,W_MAX do
		if ( wArray[i].player ~= L"" ) then
			deaths = Trakario.GetDeathCountByName( wArray[i].player )
			if (deaths > wArray[i].deaths) then
				UnregisterEventHandler(SystemData.Events.UPDATE_PROCESSED, "Trakario.MB3FlagDrop")
				dropuser = wArray[i].player
				break
			end
		end
	end

	if (dropuser ~= L"") then
		DropCarrier( dropuser )
		Trakario.Log( "[MB3FlagDrop] Exit return true" )
		return true		
	end
	
	Trakario.Log( "[MB3FlagDrop] Exit return nil" )
	return nil
end

-----------------------------------------------------
function Trakario.MB3FlagCarrier()
-----------------------------------------------------
	local aType = SystemData.AlertText.VecType[1]
	local aText = SystemData.AlertText.VecText[1]
	Trakario.Log( "[MB3FlagCarrier] Enter" )

	if ( aType == Trakario.Alert ) then
		Trakario.Log( "[MB3FlagCarrier] Alert="..tostring(aText) )
		if ( wstring.match( aText, Trakario.BaseLang["PICKUP"] ) ) then -- flag was picked up
			local name, realm = wstring.match( aText, Trakario.BaseLang["PICKUP"] )			
			SetCarrier( realm, name, Trakario.GetCareerIDByName( name ) )
		elseif ( wstring.find( aText, Trakario.BaseLang["DROP"]) ) then -- flag was dropped
			--FireTheMb3FlagDrop function but a short time after the drop occured, because the scoreboard isn updated yet
            DelayTime = 0;
			RegisterEventHandler(SystemData.Events.UPDATE_PROCESSED, "Trakario.MB3FlagDrop")
		end		
	end

	Trakario.Log( "[MB3FlagCarrier] Exit" )
end











