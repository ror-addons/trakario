local fcarrier, fcareer, frealm
local AnnouncedTime = 0;
local ANNOUNCE_DELAY = 1
local timetoannounce = ANNOUNCE_DELAY

local function SetCarrier( realm, carrier, careerId ) -- set up carrier windows
	Trakario.Log( "[BRSetCarrier] Enter realm="..tostring(realm).." carrier="..tostring(carrier).." careerId="..tostring(careerId) )
	local labelColor, flagTex
	fcarrier, fcareer, frealm = carrier, careerId, Trakario.StringRealmToID( realm )

	if ( frealm == GameData.Realm.ORDER ) then
		labelColor = DefaultColor.RealmColors[1]
		flagTex = "FlagOrder"
	elseif ( frealm == GameData.Realm.DESTRUCTION ) then
		labelColor = DefaultColor.RealmColors[2]
		flagTex = "FlagDestruction"
	else
		labelColor = DefaultColor.RealmColors[0]
		flagTex = "FlagNeutral"
	end
	
	Trakario.Log( "[BRSetCarrier] flagTex="..flagTex )
	DynamicImageSetTextureSlice( Trakario.Window.."OrderIcon", flagTex )
	LabelSetText( Trakario.Window.."OrderText", fcarrier )
	LabelSetTextColor( Trakario.Window.."OrderText", labelColor.r, labelColor.g, labelColor.b )
	Trakario.SetCareerIcon( Trakario.Window.."OrderIcon2", fcareer )
	Trakario.Log( "[BRSetCarrier] Exit" )
end

-----------------------------------------------------
function Trakario.StartBR()
-----------------------------------------------------
	Trakario.Log( "[StartBR] Enter" )
	-- populate windows with initial data
	SetCarrier( L"neutral", L"", nil )
	Trakario.RegisterTempEvent( SystemData.Events.SHOW_ALERT_TEXT, "Trakario.BRFlagCarrier" )
	Trakario.RegisterTempEvent( SystemData.Events.UPDATE_PROCESSED, "Trakario.BRUpdateTime")
	
	-- Show the Order frame, we use it in this scenario
	WindowSetShowing( Trakario.Window.."Order", true )
    
    --reset AnnouncedTime
    AnnouncedTime = 0
	Trakario.Log( "[StartBR] Exit" )
end

-----------------------------------------------------
function Trakario.BRUpdateTime(elapsed)
-----------------------------------------------------
	timetoannounce = timetoannounce - elapsed
    if timetoannounce > 0 then
        return
    end
    timetoannounce = ANNOUNCE_DELAY
	
	if (AnnouncedTime == 0) then
		LabelSetText( Trakario.Window.."OrderTime", L"" )			
	else 
	    local timeLeft = AnnouncedTime - GameData.ScenarioData.timeLeft
	    local text = TimeUtils.FormatClock(90 - timeLeft)
	    LabelSetText( Trakario.Window.."OrderTime", text )	
	end

end


-----------------------------------------------------
function Trakario.BRFlagCarrier()
-----------------------------------------------------
	local aType = SystemData.AlertText.VecType[1]
	local aText = SystemData.AlertText.VecText[1]
	Trakario.Log( "[BRFlagCarrier] Enter" )

	if ( aType == Trakario.Alert ) then
		Trakario.Log( "[BRFlagCarrier] Alert="..tostring(aText) )
		if ( wstring.match( aText, Trakario.BaseLang["PICKUP"]) ) then -- flag was picked up
			local name, realm = wstring.match( aText, Trakario.BaseLang["PICKUP"] )			
			SetCarrier( realm, name, Trakario.GetCareerIDByName( name ) )
			AnnouncedTime = GameData.ScenarioData.timeLeft
		elseif ( wstring.find( aText, Trakario.BaseLang["DROP"] ) ) then -- flag was reset to base
			SetCarrier( L"neutral", L"", nil )
			AnnouncedTime = 0
		elseif ( wstring.find( aText, Trakario.BaseLang["BOMBED"] ) ) then -- flag was reset to base
			SetCarrier( L"neutral", L"", nil )			
			AnnouncedTime = 0
		else
			for key, value in ipairs( Trakario.BaseLang["RESET"] ) do	
				if ( wstring.find( aText, value ) ) then -- flag was reset to base
					SetCarrier( L"neutral", L"", nil )
					AnnouncedTime = 0
				end
			end
		end		
	end
	Trakario.Log( "[BRFlagCarrier] Exit" )
end
