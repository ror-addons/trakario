local ocarrier, ocareer
local dcarrier, dcareer

local function SetCarrier( realm, carrier, careerId ) -- setup carrier windows 
	Trakario.Log( "[CTFSetCarrier] Enter realm="..tostring(realm).." carrier="..tostring(carrier).." careerId="..tostring(careerId) )
	local labelColor, flagTex
	local realmId = Trakario.StringRealmToID( realm )
	if ( carrier == L"_" ) then
		carrier = L""
		flagTex = "FlagNeutral"
	end
		
	if ( realmId == GameData.Realm.ORDER ) then
		ocarrier, ocareer = carrier, careerId
		if ( flagTex ~= "FlagNeutral" ) then
			flagTex = "FlagOrder"
		end
		labelColor = DefaultColor.RealmColors[1]
		Trakario.Log( "[CBSetCarrier] flagTex="..flagTex )	
		DynamicImageSetTextureSlice( Trakario.Window.."OrderIcon", flagTex )
		LabelSetText( Trakario.Window.."OrderText", ocarrier )
		LabelSetTextColor( Trakario.Window.."OrderText", labelColor.r, labelColor.g, labelColor.b )
		Trakario.SetCareerIcon( Trakario.Window.."OrderIcon2", ocareer )
	else
		dcarrier, dcareer = carrier, careerId
		if ( flagTex ~= "FlagNeutral" ) then
			flagTex = "FlagDestruction"
		end
		labelColor = DefaultColor.RealmColors[2]
		Trakario.Log( "[CBSetCarrier] flagTex="..flagTex )
		DynamicImageSetTextureSlice( Trakario.Window.."DestructionIcon", flagTex )
		LabelSetText( Trakario.Window.."DestructionText", dcarrier )
		LabelSetTextColor( Trakario.Window.."DestructionText", labelColor.r, labelColor.g, labelColor.b )
		Trakario.SetCareerIcon( Trakario.Window.."DestructionIcon2", dcareer )
	end
	Trakario.Log( "[CBSetCarrier] Exit" )
end

-----------------------------------------------------
function Trakario.StartCTF()
-----------------------------------------------------
	Trakario.Log( "[StartCTF] Enter" )
	-- populate windows with initial data
	SetCarrier( L"Order", L"_", nil )
	SetCarrier( L"Destruction", L"_", nil )
	Trakario.RegisterTempEvent( SystemData.Events.SHOW_ALERT_TEXT, "Trakario.CTFFlagCarrier" )

	-- Show the both frames, we use them in this scenario
	WindowSetShowing( Trakario.Window.."Order", true )
	WindowSetShowing( Trakario.Window.."Destruction", true )
	Trakario.Log( "[StartCTF] Exit" )
end
	
-----------------------------------------------------
function Trakario.CTFFlagCarrier()
-----------------------------------------------------
	local aType = SystemData.AlertText.VecType[1]
	local aText = SystemData.AlertText.VecText[1]

	if ( aType == Trakario.Alert ) then
		Trakario.Log( "[BRFlagCarrier] Alert="..tostring(aText) )
		EA_ChatWindow.Print(L"[Trakario] Alert: "..aText )
		if ( wstring.match( aText, Trakario.BaseLang["PICKUP"] ) ) then -- flag was picked up
			local name, realm = wstring.match( aText, Trakario.BaseLang["PICKUP"] )
			SetCarrier( realm, name, Trakario.GetCareerIDByName( name ) )
		elseif ( wstring.find( aText, Trakario.BaseLang["CAPTURE"] ) ) then -- flag was captured, reset all
			SetCarrier( L"Order", L"_", nil )
			SetCarrier( L"Destruction", L"_", nil )
		elseif ( wstring.match( aText, Trakario.BaseLang["DROP"]) ) then -- flag was dropped
			local realm = wstring.match( aText, Trakario.BaseLang["DROP"] )
			realm = Trakario.StringFlagToRealm(realm)
			-- Have to flipflop the realm on drops, since the flags are named after opponents
			if ( realm == L"Order" ) then
				realm = L"Destruction"
			else
				realm = L"Order"
			end
			SetCarrier( realm, L"_", nil )
		end
	end
	Trakario.Log( "[CTFFlagCarrier] Exit" )
end
	