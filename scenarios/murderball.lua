local fcarrier, fcareer, frealm

local function SetCarrier( realm, carrier, careerId ) -- set up carrier windows
	Trakario.Log( "[MBSetCarrier] Enter realm="..tostring(realm).." carrier="..tostring(carrier).." careerId="..tostring(careerId) )
	local labelColor, flagTex
	fcarrier, fcareer, frealm = carrier, careerId, Trakario.StringRealmToID( realm )

	if ( frealm == GameData.Realm.ORDER ) then
		labelColor = DefaultColor.RealmColors[1]
		flagTex = "FlagOrder"
	elseif ( frealm == GameData.Realm.DESTRUCTION ) then
		labelColor = DefaultColor.RealmColors[2]
		flagTex = "FlagDestruction"
	else
		labelColor = DefaultColor.RealmColors[0]
		flagTex = "FlagNeutral"
	end
	
	Trakario.Log( "[MBSetCarrier] flagTex="..flagTex )
	DynamicImageSetTextureSlice( Trakario.Window.."OrderIcon", flagTex )
	LabelSetText( Trakario.Window.."OrderText", fcarrier )
	LabelSetTextColor( Trakario.Window.."OrderText", labelColor.r, labelColor.g, labelColor.b )
	Trakario.SetCareerIcon( Trakario.Window.."OrderIcon2", fcareer )
	Trakario.Log( "[MBSetCarrier] Exit" )
end

-----------------------------------------------------
function Trakario.StartMB()
-----------------------------------------------------
	Trakario.Log( "[StartMB] Enter" )
	-- populate windows with initial data
	SetCarrier( L"neutral", L"", nil )
	Trakario.RegisterTempEvent( SystemData.Events.SHOW_ALERT_TEXT, "Trakario.MBFlagCarrier" )
	
	-- Show the Order frame, we use it in this scenario
	WindowSetShowing( Trakario.Window.."Order", true )
	Trakario.Log( "[StartMB] Exit" )
end

-----------------------------------------------------
function Trakario.MBFlagCarrier()
-----------------------------------------------------
	local aType = SystemData.AlertText.VecType[1]
	local aText = SystemData.AlertText.VecText[1]
	Trakario.Log( "[MBFlagCarrier] Enter" )

	if ( aType == Trakario.Alert ) then
		Trakario.Log( "[MBFlagCarrier] Alert="..tostring(aText) )
		if ( wstring.match( aText, Trakario.BaseLang["PICKUP"] ) ) then -- flag was picked up
			local name, realm = wstring.match( aText, Trakario.BaseLang["PICKUP"] )			
			SetCarrier( realm, name, Trakario.GetCareerIDByName( name ) )
		elseif ( wstring.match( aText, Trakario.BaseLang["RETRIEVE"] ) ) then -- flag was retrieved up
			local name, realm = wstring.match( aText, Trakario.BaseLang["RETRIEVE"] )			
			SetCarrier( realm, name, Trakario.GetCareerIDByName( name ) )
		elseif ( wstring.find( aText, Trakario.BaseLang["DROP"] ) ) then -- flag was dropped
			SetCarrier( L"neutral", L"DROPPED!", nil )
		else
			for key, value in ipairs( Trakario.BaseLang["RESET"] ) do	
				if ( wstring.find( aText, value ) ) then -- flag was reset to base
					SetCarrier( L"neutral", L"", nil )
				end
			end
		end		
	end
	Trakario.Log( "[MBFlagCarrier] Exit" )
end
