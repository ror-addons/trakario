Trakario = {}

Trakario.title = "Trakario"
Trakario.Version = L"r14"
Trakario.Window = "TrakarioWindow"

Trakario.Alert = SystemData.AlertText.Types.DEFAULT

Trakario.TempEvents = {} -- stores registered events for running scenario

Trakario.Scenarios = {}

-----------------------------------------------------
function Trakario.InitLang()
-----------------------------------------------------
	--inits lang and mapping what scenario is what type of scenarios
	Trakario.Scenarios = {}
	
	-- Adding them again by id should hopefully work on all languages :D
	Trakario.Scenarios[GetScenarioName( 2001 )] = "murderball"	--Mourkain Temple
	Trakario.Scenarios[GetScenarioName( 2101 )] = "murderball"	--Stonetroll Crossing
	Trakario.Scenarios[GetScenarioName( 2201 )] = "ctf"			--Phoenix Gate
	Trakario.Scenarios[GetScenarioName( 2202 )] = "murderball"	--Tor Anroc
	Trakario.Scenarios[GetScenarioName( 2203 )] = "murderball"	--Lost Temple of Isha
	Trakario.Scenarios[GetScenarioName( 2002 )] = "capballB"    --Black Fire Basin	 (using a special variant of capball)
	Trakario.Scenarios[GetScenarioName( 2102 )] = "bombrun"		--Talabec Dam
	Trakario.Scenarios[GetScenarioName( 2011 )] = "murderball3"	--Doomfist Crater
	Trakario.Scenarios[GetScenarioName( 2204 )] = "capball"		--Serpent's Passage
	Trakario.Scenarios[GetScenarioName( 2104 )] = "murderball3"	--Maw of Madness
	Trakario.Scenarios[GetScenarioName( 2107 )] = "ctf"			--Grovod Caverns
	Trakario.Scenarios[GetScenarioName( 2008 )] = "bombrun"		--Howling Gorge	
	Trakario.Scenarios[GetScenarioName( 2206 )] = "rkoth"		--Blood of the Black Cairn
	Trakario.Scenarios[GetScenarioName( 2109 )] = "rkoth"		--Reikland Hills

	UnloadStringTable( "Hardcoded" )
	LoadStringTable("Hardcoded", "data/strings/<LANG>", "hardcoded.txt", "cache/<LANG>", "StringTables.HARDCODED" ) 

	Trakario.BaseLang = {}
	Trakario.BaseLang["PICKUP"] = GetFormatStringFromTable("Hardcoded",StringTables.HARDCODED.TEXT_FLAG_PICKUP,{ L"(.+)", L"(.+)" , L"(.+)"})
	Trakario.BaseLang["DROP"] = GetFormatStringFromTable("Hardcoded",StringTables.HARDCODED.TEXT_FLAG_DROP,{ L"(.+)" })
	Trakario.BaseLang["RETRIEVE"] = GetFormatStringFromTable("Hardcoded",StringTables.HARDCODED.TEXT_FLAG_RETRIEVE,{ L"(.+)", L"(.+)" , L"(.+)"})
	Trakario.BaseLang["CAPTURE"] = GetFormatStringFromTable("Hardcoded",StringTables.HARDCODED.TEXT_FLAG_CAPTURE,{ L"(.+)", L"(.+)" , L"(.+)"})
	Trakario.BaseLang["BOMBED"] = GetFormatStringFromTable("Hardcoded",StringTables.HARDCODED.TEXT_BOMB_CAPTURE,{ L"(.+)", L"(.+)" , L"(.+)"})
	
		
	--for debug; /script d(Trakario.BaseLang)
	--some special replacements for language spezific stuff
	
	--german
	if SystemData.Settings.Language.active == SystemData.Settings.Language.GERMAN then
		Trakario.BaseLang["PICKUP"] = wstring.gsub(Trakario.BaseLang["PICKUP"],L"vom",L"von")
		Trakario.BaseLang["PICKUP"] = wstring.gsub(Trakario.BaseLang["PICKUP"],L"den ",L"")
		Trakario.BaseLang["DROP"] = wstring.gsub(Trakario.BaseLang["DROP"],L"Der ",L"")
		Trakario.BaseLang["RETRIEVE"] = wstring.gsub(Trakario.BaseLang["RETRIEVE"],L"vom",L"von")
		Trakario.BaseLang["RETRIEVE"] = wstring.gsub(Trakario.BaseLang["RETRIEVE"],L"den ",L"")
		Trakario.BaseLang["CAPTURE"] = wstring.gsub(Trakario.BaseLang["CAPTURE"],L"vom",L"von")
		Trakario.BaseLang["CAPTURE"] = wstring.gsub(Trakario.BaseLang["CAPTURE"],L"den ",L"")
		Trakario.BaseLang["BOMBED"] = wstring.gsub(Trakario.BaseLang["BOMBED"],L"vom",L"von")
		Trakario.BaseLang["BOMBED"] = wstring.gsub(Trakario.BaseLang["BOMBED"],L"den ",L"")
	end
	
	--french
	if SystemData.Settings.Language.active == SystemData.Settings.Language.FRENCH then
		Trakario.BaseLang["PICKUP"] = wstring.gsub(Trakario.BaseLang["PICKUP"],L"de ",L"d.")
		Trakario.BaseLang["PICKUP"] = wstring.gsub(Trakario.BaseLang["PICKUP"],L"le ",L"")
		Trakario.BaseLang["DROP"] = wstring.gsub(Trakario.BaseLang["DROP"],L"le ",L"")
		Trakario.BaseLang["DROP"] = wstring.gsub(Trakario.BaseLang["DROP"],L"de ",L"d.")
		Trakario.BaseLang["RETRIEVE"] = wstring.gsub(Trakario.BaseLang["RETRIEVE"],L"de ",L"d.")
		Trakario.BaseLang["RETRIEVE"] = wstring.gsub(Trakario.BaseLang["RETRIEVE"],L"le ",L"")
		Trakario.BaseLang["CAPTURE"] = wstring.gsub(Trakario.BaseLang["CAPTURE"],L"de ",L"d.")
		Trakario.BaseLang["CAPTURE"] = wstring.gsub(Trakario.BaseLang["CAPTURE"],L"le ",L"")
		Trakario.BaseLang["BOMBED"] = wstring.gsub(Trakario.BaseLang["BOMBED"],L"de ",L"d.")
		Trakario.BaseLang["BOMBED"] = wstring.gsub(Trakario.BaseLang["BOMBED"],L"le ",L"")
	end
	
	--spanish
	if SystemData.Settings.Language.active == SystemData.Settings.Language.SPANISH then
		Trakario.BaseLang["PICKUP"] = wstring.gsub(Trakario.BaseLang["PICKUP"],L"de ",L"d.")
		Trakario.BaseLang["RETRIEVE"] = wstring.gsub(Trakario.BaseLang["RETRIEVE"],L"de ",L"d.")
		Trakario.BaseLang["CAPTURE"] = wstring.gsub(Trakario.BaseLang["CAPTURE"],L"de ",L"d.")
		Trakario.BaseLang["BOMBED"] = wstring.gsub(Trakario.BaseLang["BOMBED"],L"de ",L"d.")
	end
		
	--Order and Destruction names in all Languages. will be used to detect flagnames and realm from alerts
	--We need the raw names and cant load the names based stringstables because flagnames are serverside!
	Trakario.BaseLang["ORDER"] = { L"Order",L"Ordre",L"Ordnung",L"Ordine",L"Orden"}
	Trakario.BaseLang["DESTRUCTION"] = { L"Destruction",L"Destruction",L"Zerst�rung",L"Distruzione",L"Destrucci�n"}
	
	--Same here Resetmessages are Serverside, a list for all languages are listed here
	Trakario.BaseLang["RESET"] = { L"reset", L"r�initialis�e", L"zur�ckgesetzt", L"resettate", L"reiniciado"}
end

-- Called when Addon is Loaded
-----------------------------------------------------
function Trakario.OnInitialize()
-----------------------------------------------------
	Trakario.LoggingInit()
	Trakario.Log( "[OnInitialize] Logging Initialized" )
	Trakario.SetupWindow()
	Trakario.RegisterSelfEvents()

	--EA_ChatWindow.Print( L"Trakario version " .. Trakario.Version .. L" has loaded!" )
	Trakario.Log( "* * * Trakario version "..tostring(Trakario.Version).." has loaded * * *" )
	
	Trakario.InitLang()
	
	-- Check to see if we are initializing while already in a scenario
	if ( GameData.Player.isInScenario ) then
		EA_ChatWindow.Print( L"[Trakario] Emergency Boot!" )
		Trakario.Log( "[Trakario] Emergency Boot!" )
		Trakario.SCENARIO_BEGIN()
	end
	Trakario.Log( "[OnInitialize] Exit" )
end

-----------------------------------------------------
function Trakario.RegisterSelfEvents()
-----------------------------------------------------
	Trakario.Log( "[RegisterSelfEvents] Enter" )
	RegisterEventHandler( SystemData.Events.SCENARIO_BEGIN, "Trakario.SCENARIO_BEGIN" )
	RegisterEventHandler( SystemData.Events.SCENARIO_END, "Trakario.SCENARIO_END" )
	Trakario.Log( "[RegisterSelfEvents] Exit" )
end

-----------------------------------------------------
function Trakario.RegisterTempEvent( event, func )
-----------------------------------------------------
	Trakario.Log( "[RegisterTempEvent] Enter event="..tostring( event ).." func="..func )
	Trakario.TempEvents[event] = func
	RegisterEventHandler( event, func )
	Trakario.Log( "[RegisterTempEvent] Exit" )
end

-----------------------------------------------------
function Trakario.UnregisterSelfEvents()
-----------------------------------------------------
	Trakario.Log( "[UnRegisterSelfEvents] Enter" )
	UnregisterEventHandler( SystemData.Events.SCENARIO_BEGIN, "Trakario.SCENARIO_BEGIN" )
	UnregisterEventHandler( SystemData.Events.SCENARIO_END, "Trakario.SCENARIO_END" )
	Trakario.Log( "[UnRegisterSelfEvents] Exit" )
end

-----------------------------------------------------
function Trakario.UnregisterTempEvents()
-----------------------------------------------------
	Trakario.Log( "[UnregisterTempEvents] Enter" )
	for event, func in pairs( Trakario.TempEvents ) do
		Trakario.Log( "[UnregisterTempEvents] event="..tostring( event ).." func="..func )
		UnregisterEventHandler( event, func )
		Trakario.TempEvents[event] = nil
	end
	Trakario.Log( "[UnregisterTempEvents] Exit" )
end

-----------------------------------------------------
function Trakario.SetupWindow()
-----------------------------------------------------
	Trakario.Log( "[SetupWindow] Enter" )
	CreateWindow( Trakario.Window, false )
	WindowSetShowing(Trakario.Window.."Order", false)
	WindowSetShowing(Trakario.Window.."Destruction", false)
	WindowSetShowing(Trakario.Window.."Third", false)	
	LayoutEditor.RegisterWindow( Trakario.Window, L"Trakario", L"Track Scenario Flag Carriers", false, false, true, nil )
	Trakario.Log( "[SetupWindow] Exit" )
end

-----------------------------------------------------
function Trakario.SCENARIO_BEGIN()
-----------------------------------------------------
	Trakario.Log( "[SCENARIO_BEGIN] Enter" )
	WindowSetShowing(Trakario.Window, true)
	Trakario.ScenarioCheck()
	Trakario.Log( "[SCENARIO_BEGIN] Exit" )
end

-----------------------------------------------------
function Trakario.Test()
-----------------------------------------------------
	WindowSetShowing(Trakario.Window, true)
	labelColor = DefaultColor.RealmColors[1]
	DynamicImageSetTextureSlice( Trakario.Window.."Tracker1Icon", "FlagDestruction" )
	LabelSetText( Trakario.Window.."Tracker1Text", L"Lolinger" )
	LabelSetTextColor( Trakario.Window.."Tracker1Text", labelColor.r, labelColor.g, labelColor.b )
	Trakario.SetCareerIcon( Trakario.Window.."Tracker1Icon2", 1 )
	LabelSetText( Trakario.Window.."Tracker1Time", L"10:00" )
end

-----------------------------------------------------
function Trakario.SCENARIO_END()
-----------------------------------------------------
	Trakario.Log( "[SCENARIO_END] Enter" )
	WindowSetShowing(Trakario.Window.."Order", false)
	WindowSetShowing(Trakario.Window.."Destruction", false)
	WindowSetShowing(Trakario.Window.."Third", false)	
	WindowSetShowing(Trakario.Window, false)
	Trakario.UnregisterTempEvents()
	LabelSetText( Trakario.Window.."OrderTime", L"" )	
	LabelSetText( Trakario.Window.."DestructionTime", L"" )	
	LabelSetText( Trakario.Window.."ThirdTime", L"" )		
	Trakario.Log( "[SCENARIO_END] Exit" )
end

-----------------------------------------------------
function Trakario.ScenarioCheck()  -- check to see which scenario we are in
-----------------------------------------------------
	Trakario.Log( "[ScenarioCheck] Enter" )
	if ( not GameData.Player.isInScenario ) then
		return
	end

	local name = GetScenarioName( GameData.ScenarioData.id  )
	
	for scenario, type in pairs( Trakario.Scenarios ) do
		Trakario.Log( "[ScenarioCheck] s='"..tostring(scenario).."', '"..tostring(name).."'" )
		if ( scenario == name ) then
			EA_ChatWindow.Print( L"[Trakario] Entering Scenario: "..name )
			Trakario.Log( "[ScenarioCheck] t="..type )
			if ( type == "murderball" ) then
				Trakario.StartMB()
				break
			elseif ( type == "murderball3" ) then
				Trakario.StartMB3()
				break
			elseif ( type == "bombrun" ) then
				Trakario.StartBR()
				break
			elseif ( type == "capball" ) then
				Trakario.StartCB()
				break
			elseif ( type == "capballB" ) then
				Trakario.StartCB(1)
				break                
			elseif ( type == "ctf" ) then
				Trakario.StartCTF()
				break
			elseif ( type == "rkoth" ) then
				Trakario.StartRKOTH()
				break				
			end
		end
	end
	Trakario.Log( "[ScenarioCheck] Exit" )
end

-----------------------------------------------------
function Trakario.GetCareerIDByName( name )
-----------------------------------------------------
	Trakario.Log( "[GetCareerIDByName] Enter name="..tostring(name) )
	local interimPlayerData = GameData.GetScenarioPlayers()
	if ( interimPlayerData ~= nil ) then
		for key, value in ipairs( interimPlayerData ) do
			local playerName = wstring.match( value.name, L"(.+)(%^)(.+)" )
			if ( playerName == name ) then
				Trakario.Log( "[GetCareerIDByName] Exit return="..tostring( value.careerId ) )
				return value.careerId
			end
		end
	end
	Trakario.Log( "[GetCareerIDByName] Exit return nil" )
	return nil
end

-----------------------------------------------------
function Trakario.GetDeathCountByName( name )
-----------------------------------------------------
	Trakario.Log( "[GetDeathCountByName] Enter name="..tostring(name) )
	BroadcastEvent( SystemData.Events.SCENARIO_START_UPDATING_PLAYERS_STATS)
	local interimPlayerData = GameData.GetScenarioPlayers()
	if not WindowGetShowing("ScenarioSummaryWindow") then
		BroadcastEvent( SystemData.Events.SCENARIO_STOP_UPDATING_PLAYERS_STATS)
	end
	if ( interimPlayerData ~= nil ) then
		for key, value in ipairs( interimPlayerData ) do
			local playerName = wstring.match( value.name, L"(.+)(%^)(.+)" )
			if ( playerName == name ) then
				Trakario.Log( "[GetDeathCountByName] Exit return="..tostring( value.deaths ) )
				return value.deaths
			end
		end
	end
	Trakario.Log( "[GetDeathCountByName] Exit return nil" )
	return nil
end

-----------------------------------------------------
function Trakario.SetCareerIcon( window, careerId )
-----------------------------------------------------
	Trakario.Log( "[SetCareerIcon] Enter window="..window.." careerId="..tostring( careerId ) )
	if ( careerId == nil ) or ( careerId <= 0 ) then
		DynamicImageSetTexture( window, "", 0, 0 )
		return
	end
	local texture, x, y = GetIconData( Icons.GetCareerIconIDFromCareerNamesID( careerId ) )
	Trakario.Log( "[SetCareerIcon] texture="..texture)
	DynamicImageSetTexture( window, texture, x, y )
	Trakario.Log( "[SetCareerIcon] Exit" )
end

-----------------------------------------------------
function Trakario.StringRealmToID( realm )
-----------------------------------------------------
	Trakario.Log( "[StringRealmToID] Enter realm="..tostring(realm) )
	
	--check order
	for key, value in ipairs( Trakario.BaseLang["ORDER"] ) do	
		if (wstring.find(realm,value)) then
			Trakario.Log( "[StringRealmToID] Exit return="..tostring( GameData.Realm.ORDER ) )
			return GameData.Realm.ORDER
		end
	end
	--check destruction
	for key, value in ipairs( Trakario.BaseLang["DESTRUCTION"] ) do	
		if (wstring.find(realm,value)) then
			Trakario.Log( "[StringRealmToID] Exit return="..tostring( GameData.Realm.DESTRUCTION ) )
			return GameData.Realm.DESTRUCTION
		end
	end

	Trakario.Log( "[StringRealmToID] Exit return="..tostring( GameData.Realm.NONE ) )
	return GameData.Realm.NONE
end

-----------------------------------------------------
function Trakario.StringFlagToRealm( realm )
-----------------------------------------------------
	Trakario.Log( "[StringFlagToRealm] Enter realm="..tostring(realm) )
	
	local realmid = Trakario.StringRealmToID( realm )

	if (realmid == GameData.Realm.ORDER) then
		Trakario.Log( "[StringFlagToRealm] Exit return=Order" )
		return L"Order"
	elseif (realmid == GameData.Realm.DESTRUCTION) then
		Trakario.Log( "[StringFlagToRealm] Exit return=Destruction" )
		return L"Destruction"
	else
		Trakario.Log( "[StringFlagToRealm] Exit return=nil" )
		return nil
	end

end

-----------------------------------------------------
function Trakario.LoggingInit()
-----------------------------------------------------
	TextLogDestroy("TrakLog")
	TextLogCreate("TrakLog",9000)
	TextLogSetIncrementalSaving( "TrakLog", true, L"logs/traklog.log" )
	TextLogSetEnabled( "TrakLog", true )
	TextLogClear( "TrakLog" )
end

-----------------------------------------------------
function Trakario.Log( string )
-----------------------------------------------------
	TextLogAddEntry( "TrakLog", SystemData.ChatLogFilters.SAY, towstring( tostring( string ) ) )
end

-----------------------------------------------------
function Trakario.OnTargetPlayer()
-----------------------------------------------------
	-- get info from window
	local name = LabelGetText( SystemData.MouseOverWindow.name.."Text" )
    if (name ~= nil) and  (name ~= L"") then
    
        SystemData.UserInput.ChatText = L"/target "..name
        BroadcastEvent( SystemData.Events.SEND_CHAT_TEXT )
    end
end

function Trakario.ClearWindow()
--[[
	local name = LabelGetText( SystemData.MouseOverWindow.name.."Text" )
	Trakario.DropCarrier( name )
]]--
end
			

	