<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="Trakario" version="r13" date="08/20/2009" >

		<Author name="Fayce/smurfy" email="" />
		<Description text="Track Scenario Flag Carriers" />
        
        <VersionSettings gameVersion="1.4.1" windowsVersion="1.0" savedVariablesVersion="1.0" />

		<Dependencies>
			<Dependency name="EASystem_LayoutEditor" />
			<Dependency name="EA_ChatWindow" />
		</Dependencies>

		<Files>
			<File name="trakario.xml" />
			<File name="trakario.lua" />
			<File name="scenarios\bombrun.lua" />
			<File name="scenarios\capball.lua" />
			<File name="scenarios\CTF.lua" />
			<File name="scenarios\murderball3.lua" />
			<File name="scenarios\murderball.lua" />
			<File name="scenarios\rkoth.lua" />
		</Files>

		<OnInitialize>
			<CallFunction name="Trakario.OnInitialize" />
		</OnInitialize>

		<OnUpdate/>

		<OnShutdown>
			<CallFunction name="Trakario.UnregisterSelfEvents" />
		</OnShutdown>

	</UiMod>
</ModuleFile>

	